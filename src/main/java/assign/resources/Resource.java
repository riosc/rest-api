package assign.resources;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import assign.services.Source;

@Path("/projects")
public class Resource {
	
	Source src = new Source();
	
	@GET
	@Path("/{project}/meetings/{year}")
	@Produces("application/xml")
	public Response printMeetingYear(@PathParam("project") String project, @PathParam("year") String year) throws IOException{
		String result = src.getProjectYear(project, year);
		return Response.status(200).entity(result).build();
	}
	
	@GET
	@Path("/{project}/irclogs")
	@Produces("application/xml")
	public Response printIrclogs(@PathParam("project") String project) throws IOException{
		String result = src.getProject(project);
		return Response.status(200).entity(result).build();
	}
	
	@GET
	@Path("/")
	@Produces("application/xml")
	public Response printUnion() throws IOException{
		String result = src.getUnion(); 
		return Response.status(200).entity(result).build();
	}
}
