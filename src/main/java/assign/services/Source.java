package assign.services;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Source {
	private String url = "http://eavesdrop.openstack.org/"; 	

	public String getProjectYear(String project, String year) throws IOException{

		String url = this.url;
		url += "meetings/" + project + "/" + year;		
		Document doc = Jsoup.connect(url).get();

		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		xml.append(" <project name=\'" + project + "'>");

		Elements links = doc.select("a[href*=" + project + "." + year + "]");
		for(Element link: links){
			xml.append(" <link>" + link.attr("abs:href") + "</link>");
		}

		xml.append(" </project>");

		return xml.toString();		
	}

	public String getProject(String project) throws IOException{
		String url = this.url;
		url += "irclogs" + "/%23" + project.substring(1, project.length());		
		Document doc = Jsoup.connect(url).get();

		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		xml.append("<project name='" + project.substring(1, project.length()) + "'>");

		Elements links = doc.select("a[href*=%23]");
		for(Element link: links){
			xml.append("<link>" + link.attr("abs:href") + "</link>");
		}

		xml.append("</project>");

		return xml.toString(); 
	}

	public String getUnion() throws IOException{
		String url = this.url;
		url += "irclogs";
		Document doc = Jsoup.connect(url).get();

		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		xml.append("<projects>");

		Elements links = doc.select("a[href*=%23]");
		for(Element link: links){
			String ln = link.attr("abs:href");
			xml.append("<project>#" + ln.substring(ln.indexOf("%23")+3, ln.length()-1) + "</project>");
		}

		url = this.url;
		url += "meetings";
		int i = 0;

		Document doc2 = Jsoup.connect(url).get();

		Elements correct_doc = doc2.select("a[href]");
		for(Element correct_link: correct_doc){
			i++; correct_link.attr("abs:href");
			if(i > 5){
				String ln = correct_link.attr("abs:href");
				xml.append("<project>" + ln.substring(ln.indexOf("meetings")+9, ln.length()-1) + "</project>");
			}
		}

		xml.append("</projects>");

		return xml.toString();
	}
}
