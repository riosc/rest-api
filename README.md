In this assignment I created a REST API using RESTEasy that would query http://eavesdrop.openstack.org/ and get the requested project with all of its details. This project was used to show understanding of the GET functionality. The queried information was displayed back as xml.  

-Cesar Rios